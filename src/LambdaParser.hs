{-# LANGUAGE OverloadedStrings #-}

module LambdaParser where

import Control.Monad
import Data.Text (Text)
import Data.Void
--import Data.List (intercalate)
import Text.Megaparsec hiding (State)
import Text.Megaparsec.Char
import qualified Data.Text as T
import qualified Text.Megaparsec.Char.Lexer as L





doParse :: Text -> IO()
doParse t = parseTest parseTerm t

parseText :: Text -> Either (ParseErrorBundle Text Void) Term
parseText t = runParser parseUntilEmpty "" t

type ParseError = ParseErrorBundle Text Void
type Parser = Parsec Void Text
--type Parser = Parsec ParseError Text

data Term = Var Text
          | Lambda [Term] Term
          | Application Term Term
          deriving (Eq, Show)


--myfoldl :: Foldable t => (b -> a -> b) -> t a -> b
--myfoldl f (x:xs) = foldl f x xs

applicator :: [Term] -> Term
applicator (t:[]) = t
applicator (t:ts) = foldl Application t ts
applicator [] = error "There are no terms in the expression"

parseUntilEmpty :: Parser Term
parseUntilEmpty = do
  terms <- some parseTermNotApp
  return (applicator terms)

parseLambdaPrefix :: Parser Char
parseLambdaPrefix = try $ char '\\'  <|> char 'λ' -- <|> char 'L' 

spaceConsumer :: Parser ()
spaceConsumer = L.space space1 empty empty

parseTerm :: Parser Term
parseTerm = spaceConsumer *> choice [
    try parseLambda,
    try parseApplication,
    try parseVar,
    between (char '(') (char ')') parseTerm
  ]

parseTermNotApp :: Parser Term
parseTermNotApp = spaceConsumer *> choice [
    try parseLambda,
    try parseVar,
    between (char '(') (char ')') parseTerm
  ]

parseVar :: Parser Term
parseVar = do
  spaceConsumer
  notFollowedBy parseLambdaPrefix
  spaceConsumer
  varName <- T.pack <$> some letterChar
  spaceConsumer
  return (Var varName)

parseVars :: Parser [Term]
parseVars = some parseVar

parseApplication :: Parser Term
parseApplication = try $ do
  spaceConsumer
  terml <- parseTermNotApp
  spaceConsumer
  termr <- parseTerm --parseTerm
  return (Application terml termr)

parseLambda :: Parser Term
parseLambda = do
  spaceConsumer
  void parseLambdaPrefix
  variables <- parseVars
  spaceConsumer
  void (char '.')
  spaceConsumer
  Lambda variables <$> parseTerm

getVarName :: Term -> Text
getVarName (Var x) = x
getVarName _       = error "Not a variable"

termToText :: Term -> Text
termToText (Var name) = name
termToText (Lambda vars body) = "λ" <> T.intercalate " " (map getVarName vars) <> ".(" <> termToText body <> ")"
--termToText (Lambda vars body) = "\\" <> T.intercalate " " (map getVarName vars) <> ".(" <> termToText body <> ")"
termToText (Application termL termR) = termToText termL <> " " <> termToText termR
--termToText (Application termL termR) = "(" <> termToText termL <> " " <> termToText termR <> ")"


getSyntaxErrors :: Term -> [Text]
getSyntaxErrors (Application t1 t2) = getSyntaxErrors t1 ++ getSyntaxErrors t2
getSyntaxErrors term@(Lambda vars t) = varError ++ getSyntaxErrors t
  where
    errorVars = uniqeElems vars
    varError = if errorVars == [] then [] else ["non unique parameters (" <> (foldl (<>) "" (map termToText errorVars)) <> ") in term " <> termToText term]
getSyntaxErrors _ = []

uniqeElems :: (Eq a) => [a] -> [a]
uniqeElems (x:xs) = ret ++ (uniqeElems xs)
  where
    ret = if x `elem` xs then [x] else []  
uniqeElems [] = []