{-# LANGUAGE OverloadedStrings #-}

module Test where

import Test.HUnit (runTestTT)
import Test.HUnit.Base
import Data.Text (Text)


import LambdaParser (parseText, Term (Var, Application, Lambda))
import Interpreter (interpret)

getParsedTerm :: Text -> Term 
getParsedTerm text = case parseText text of
    Left _ -> error "could not parse!"
    Right term -> term

matchTerms :: Text -> Term -> Test
matchTerms textL termR = TestCase $ assertEqual "Terms do not match!" (getParsedTerm textL) termR

matchInterp :: Text -> Term -> Test
matchInterp textL termR = TestCase $ assertEqual "Interpretation does not match" (interpret $ getParsedTerm textL) termR

runTests :: IO Counts
runTests = runTestTT $ TestList [
    matchTerms "((\\x y . x) a) b" (Application (Application (Lambda [Var "x", Var "y"] (Var "x")) (Var "a")) (Var "b")),
    matchTerms "(\\x y . x) a b" (Application (Application (Lambda [Var "x", Var "y"] (Var "x")) (Var "a")) (Var "b")),
    matchTerms "(\\x y. x y) (\\z. z)" (Application (Lambda [Var "x", Var "y"] (Application (Var "x") (Var "y"))) (Lambda [Var "z"] (Var "z"))),
    matchTerms "(\\x.x x)(\\x.x x)" (Application (Lambda [Var "x"] (Application (Var "x") (Var "x"))) (Lambda [Var "x"] (Application (Var "x") (Var "x")))),
    
    matchInterp "((\\x y . x) a) b" $ Var "a",
    matchInterp "(\\x y . x) a b" $ Var "a",
    matchInterp "(\\x y. x y) (\\z. z)" (Lambda [Var "y"] $ Var "y"),
    matchInterp "(\\x.x x)(\\x.x x)" (Application (Lambda [Var "x"] (Application (Var "x") (Var "x"))) (Lambda [Var "x"] (Application (Var "x") (Var "x")))),

    matchTerms "λf.λx.x" (Lambda [Var "f"] $ Lambda [Var "x"] $ Var "x"),
    matchTerms "λf.λx.f x" (Lambda [Var "f"] $ Lambda [Var "x"] (Application (Var "f") (Var "x")))
    ]
