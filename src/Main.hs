{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE TypeApplications  #-}
-- {-# LANGUAGE DataKinds, KindSignatures #-}
-- {-# LANGUAGE BlockArguments #-}

module Main (main) where

import System.IO
import Text.Megaparsec.Error
import qualified Data.Text as T


import LambdaParser (parseText, termToText, getSyntaxErrors)
import Interpreter (interpretWithSteps)
import Test (runTests)

mainLoop :: IO ()
mainLoop = do
  hSetBuffering stdout NoBuffering
  -- putStr "Enter Lambda expression:" 
  putStr "λ> " 
  input <- T.pack <$> getLine
  if input == "!" then return () else do
    case parseText input of
      Left err -> putStrLn $ "Error: " ++ errorBundlePretty err
      Right term -> do 
          -- mapM_ (putStrLn . show) (interpretWithSteps term)
          mapM_ (putStrLn . show) (getSyntaxErrors term)
        
          --putStrLn ""
          putStr "\n"
          mapM_ (putStrLn . T.unpack . termToText) $ interpretWithSteps term
    mainLoop

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering
    mainLoop






