{-# LANGUAGE OverloadedStrings #-}


module Interpreter where

import Data.Text (Text)
-- import qualified Data.Text as T
-- import Data.List (intersperse)

import LambdaParser (Term (Var, Application, Lambda), termToText)


simplify :: Term -> Term
simplify (Lambda vars term) = Lambda vars $ simplify term
simplify (Application (Lambda (v:[]) body) term) = swapVar body v term
simplify (Application (Lambda (v:vars) body) term) = Lambda vars $ swapVar body v term
simplify (Application termL termR) = Application (simplify termL) (simplify termR)
simplify term = term


-- TermToSwap VarToSwap TermToSwapTo returnTerm
-- Term       Term(Var) Term         Term
swapVar :: Term -> Term -> Term -> Term
swapVar (Lambda vars body) var toTerm = 
  if var `elem` vars
    then Lambda vars body
    else Lambda vars $ swapVar body var toTerm
swapVar org@(Var x) (Var y) toTerm = 
  if x == y 
    then toTerm 
    else org
swapVar (Application termL termR) var toTerm = Application (swapVar termL var toTerm) (swapVar termR var toTerm)
swapVar _ _ _ = error "!ERROR!"


getReductionStep :: Term -> Term -> [Text]
getReductionStep termL termR = 
    if termL == termR 
        then [] 
        else [termToText termL <> " -> " <> termToText termR]


interpretWithSteps :: Term -> [Term]
interpretWithSteps term = 
  let term' = simplify term
  in if term' == term
        then [term]
        else [term] ++ interpretWithSteps term'
      
interpret :: Term -> Term
interpret term = 
  let term' = simplify term
  in
    if term' == term
      then term
      else interpret term'