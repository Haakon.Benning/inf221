# INF221

## Usage
* Run and build using 'cabal run' or 'cabal build'
* Write a lambda expression and the interpreter will evaluate it.
The lambda literal is either '\\' or 'λ', and the functions can be written with multiple parameters.
Spaces are needed for to bind to the right.

## Examples
```
(\x y. x y) (\z. z)

```